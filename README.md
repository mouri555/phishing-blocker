# Phishing Blocker
## 概要
本作品はアクセス先にフィッシングサイトの可能性がある場合、以下のようなポップアップを出し、利用者に警告を行うChrome拡張機能となります。  

![](./images/Phishing_Blocker_Alert.png)

フィッシングサイトの判定には利用者の閲覧履歴とアクセス先のドメイン年齢を使用しており、直近100件のアクセス先ドメインをホワイトリストとして登録し、ホワイトリスト外のドメインにアクセスする場合のみドメイン年齢を閾としたフィッシングサイト判定を行います。  
ドメイン年齢の閾値は[IBM社公開の資料](https://www.ibm.com/downloads/cas/97KYL6DG)を参考にフィッシングサイトの平均有効時間15時間を設定しています。  


## 前準備
### Google Chromeのインストール
本拡張機能はGoogle Chromeのインストールを前提としています。  
利用PCにChromeがインストールされていない場合、[Google Chromeヘルプ](https://support.google.com/chrome/answer/95346?co=GENIE.Platform%3DDesktop&hl=ja)を参考にインストールしてください。

### WhoisXMLAPIへの登録
本拡張機能ではドメイン情報(ドメイン年齢含む)を取得するためにWhoisXMLAPIを使用しています。WhoisXMLAPIを使用するために登録が必要となるため、以下に登録の手順を記載します。  

1．[WhoisXMLAPIの公式サイト](https://whois.whoisxmlapi.com/)に移動し、画面右上のSign Upをクリックします。  
![](./images/WhoisXMLAPI_Registration_1.png)

2．EmailとPasswordを入力し、Sign Upをクリックします。  
(ニュースレターの配信を希望しない場合はSubscribe to our newsletterのチェックを外してください。)  
![](./images/WhoisXMLAPI_Registration_2.png)

3．登録したメールアドレスに確認メールが送られてくるので、メール中のConfirmation linkをクリックして以下の画面に遷移した後、登録情報を入力し、ログインしてください。  
![](./images/WhoisXMLAPI_Registration_3.png)

4．以下の画面を確認できれば登録完了となります。  
![](./images/WhoisXMLAPI_Registration_4.png)


## 利用方法
1．本プログラムをダウンロードし、任意のフォルダに展開してください。  
2．解凍フォルダ以下のsrcフォルダ内にmain.jsあることを確認出来たら、jsファイル中のusername、passwordをそれぞれ""囲みでWhoisXMLAPIに登録したメールアドレス、パスワードを記入してください。  
![](./images/How_To_Use_2.png)

3．Chromeの画面右上にあるパズルアイコンをクリックし、"拡張機能を管理"を選択してください。  
![](./images/How_To_Use_3.png)

4．画面右上にデベロッパーモードを有効化するバーがあるので、クリックしてONにしてください。  
![](./images/How_To_Use_4.png)

5．画面左上に"パッケージ化されていない拡張機能を読み込む"ボタンが表示されるので、クリックした後、手順1で展開した、manifest.jsonファイルを含むフォルダを選択してください。  
![](./images/How_To_Use_5.png)

6．拡張機能の一覧に"phishing blocker"が追加されれば利用可能となり、フィッシングサイトの判定、警告を行います。  
![](./images/How_To_Use_6.png)

## 注意点
- 無償版のWhoisXMLAPIでは月に500リクエストまでの対応となるため、それ以上の利用に関しては現在保証していません。
- フィッシングサイトの疑いがあるサイトにアクセスした場合、履歴をホワイトリストとして使用している関係上、そのアクセス履歴は残りません。