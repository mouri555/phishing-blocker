
/* 拡張機能起動時動作の確認 */
chrome.tabs.executeScript(null, { file: "src/libs/jquery-3.2.1.min.js" }, function () {
    function GetHistory() {
        chrome.history.search({ text: '', maxResults: 100 }, function (data) {
            data.forEach(function (page) {
                console.log(page.url);
                white_url_lists.push(page.url);

            });
            var count = 1;
            white_url_lists.forEach(function (white_lists) {
                count++;
            });
        });
    }

    var white_url_lists = [];
    GetHistory();

    var old_url;
    var id = setInterval(function () {
        chrome.tabs.query({ active: true, lastFocusedWindow: true }, tabs => {
            var url = tabs[0].url;
            if (old_url != url) {
                var apiUrl = "https://www.whoisxmlapi.com/whoisserver/WhoisService";
                // Fill in your details
                var username = "";      //取得したユーザ名を入力してください
                if (true) {
                    var password = "";  //設定したパスワードを入力してください
                }
                var domain = extractHostDomain(url);
                var format = "JSON";
                var jsonCallback = "LoadJSON";
                var domain_age = 0;
                onPageLoad();

                
                function onPageLoad() {
                    // Use a JSON resource
                    var url = apiUrl
                        + "?domainName=" + encodeURIComponent(domain)
                        + "&username=" + encodeURIComponent(username)
                        + "&password=" + encodeURIComponent(password)
                        + "&outputFormat=" + encodeURIComponent(format);
                    // Dynamically Add a script to get our JSON data from a different
                    // server, avoiding cross origin problems.

                    var i = 0;
                    while (i < white_url_lists.length) {
                        if (white_url_lists[i].indexOf(domain) !== -1 || domain == "history" || domain == "newtab" || domain == "extensions") {

                            break;
                        }
                        i++;
                        if (i == white_url_lists.length) {

                            var xhr = new XMLHttpRequest();
                            xhr.open("GET", url, true);
                            xhr.onreadystatechange = function () {
                                if (xhr.readyState == 4) {
                                    //handle the xhr response here
                                    //console.log(xhr.responseText);
                                    LoadJSON(xhr.responseText);
                                }
                            }
                            xhr.send();
                        }
                    }

                }
                // Do something with the json result we get back
                function LoadJSON(result) {

                    // Print out a nice informative string
                    var domain_age = RecursivePrettyPrint(JSON.parse(result));

                    console.log("domain_age=" + domain_age);
                    CheckDomainAge(domain_age);
                }
                function RecursivePrettyPrint(obj) {
                    var str = "";
                    for (var x in obj) {
                        if (obj.hasOwnProperty(x)) {
                            if (x == "createdDate") {

                                var domain_age_tmp = CalculateDomainAge(obj[x]);
                                if (domain_age < domain_age_tmp) {
                                    domain_age = domain_age_tmp;
                                }
                                return obj[x];
                            }
                            if (typeof (obj[x]) == "string") { }
                            else
                                RecursivePrettyPrint(obj[x]);
                        }
                    }
                    return domain_age;
                }
                function CalculateDomainAge(age) {
                    var year = Number(age.substr(0, 4));
                    var month = Number(age.substr(5, 2));
                    var day = Number(age.substr(8, 2));
                    var hour = Number(age.substr(11, 2));
                    var minute = Number(age.substr(13, 2));
                    //minuteのみ0を代入
                    var minute = 0;
                    var domain_date = new Date(year, month - 1, day, hour, minute);
                    console.log("ドメイン取得日：" + domain_date.toLocaleString());
                    var now_date = new Date();
                    console.log("現在日：" + now_date.toLocaleString());
                    var diff = now_date.getTime() - domain_date.getTime();
                    var day_diff = diff / (1000 * 60 * 60);
                    day_diff = Math.floor(day_diff);
                    console.log("経過時間：" + day_diff + "時間");
                    return day_diff;
                }
                function CheckDomainAge(domain_age) {
                    var threshold = 15;
                    if (domain_age < threshold) {
                        alert("ドメイン年齢診断の結果、このサイトはフィッシングサイトの可能性があります。");
                        chrome.history.deleteUrl({
                            url: url
                        });
                    } else {
                        chrome.history.search({ text: '', maxResults: 1 }, function (data) {
                            white_url_lists.push(data.url);
                        });
                    }
                }
                function extractHostDomain(url) {
                    var host_domain;
                    if (url.indexOf("://") > -1) {
                        host_domain = url.split('/')[2];
                    }
                    else {
                        host_domain = url.split('/')[0];
                    }
                    host_domain = host_domain.split(':')[0];
                    return host_domain;
                }
                old_url = url;
            }
        });
    }, 3000);
});



